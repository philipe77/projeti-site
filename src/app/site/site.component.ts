import { Component, OnInit} from '@angular/core';

import { EmbedVideoService } from 'ngx-embed-video';
import { Http } from '@angular/http';
import { GlobalCasesService } from './site-shared/global-cases.service';
import { IMessage, SendEmailService } from './site-shared/send-email.service';


@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.css'],
  providers:[GlobalCasesService,SendEmailService]
})
export class SiteComponent implements OnInit {

  message: IMessage = {};
  
  constructor(private globals: GlobalCasesService,private appService: SendEmailService) {}


  //Envio de email
  sendEmail(message: IMessage) {
    this.appService.sendEmail(message).subscribe(res => {
      console.log('AppComponent Success', res);
    }, error => {
      console.log('AppComponent Error', error);
    })
  }

  ngOnInit() {
      
    }
  }
