import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {Observable} from 'rxjs/Rx';

export interface IMessage {
  nome?: string,
  sobrenome?:string,
  email?: string,
  descricao?: string
}


@Injectable()
export class SendEmailService {

  private emailUrl = '../../../../assets/email.php';

  constructor(private http: Http) {

  }

  sendEmail(message: IMessage): Observable<IMessage> | any {
    
    return this.http.post(this.emailUrl, message)
      .map(response => {
        console.log('Sending email was successfull', response);
        return response;
      }).catch(error => {
        console.log('Sending email got error', error);
        return Observable.throw(error)
      })
  }

}
