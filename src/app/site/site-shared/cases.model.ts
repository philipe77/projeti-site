export class Cases{
    id:string
    titulo:string;
    descricao_resumida:string;
    descricao:string
    data:string;
    imageOficial:string;
    imagePath:string
    imageResponsive:string;

    constructor(id:string,
                titulo:string,
                descricao_resumida,
                descricao:string,data:string,
                imageOficial:string,
                imagePath:string,
                imageResponsive:string){
        this.id = id;
        this.titulo = titulo;
        this.descricao_resumida = descricao_resumida;
        this.descricao = descricao;
        this.data = data;
        this.imageOficial = imageOficial;
        this.imagePath = imagePath;
        this.imageResponsive = imageResponsive;
    }
}