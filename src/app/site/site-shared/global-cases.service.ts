import { Injectable } from '@angular/core';
import { Cases } from './cases.model';

@Injectable()
export class GlobalCasesService {

  cases_model: Cases[] = [
    new Cases('e-commerce',
              'E-Commerce',
              `Fizemos uma consultoria completa de 
              um projeto de e-commerce e desenvolvemos uma loja online para o cliente ampliar suas vendas...`,

              `Foi prestada uma consultoria para o desenvolvimento completo de um e-commerce, desde o marketing digital da marca até o desenvolvimento do sistema de compras online.
              Isso tudo para que o comerciante direcionasse o seu projeto da melhor forma e com menor custo inicial.`,

              'Mar 23, 2017',
              '../../assets/image/img4.jpg',
              "../../assets/image/trabalho/mockups_sport43.jpeg",
              ""),

    new Cases('age',
               'Aplicativo para academia',
                `Foi desenvolvido um aplicativo integrado com o sistema da academia,
                onde os alunos poderiam ver as atividades de forma prática e ...'`,
                `Mediante a solicitação de uma academia para expandir seu público e obter mais controle com presença
                 nas suas atividades, foi desenvolvido um aplicativo integrado com sistema da academia,
                  onde os alunos poderiam ver as atividades de forma prática e agendar presença na mesma.
                  Utilizamos para otimização de custo do projeto,
                  o modelo de aplicação híbrida, onde o aplicativo fica disponível em todas as
                  plataformas com esforço reduzido para o desenvolvimento.`,

                'Jun 20, 2017',
                "../../../assets/image/vida-saude.jpg",
                "../../assets/image/trabalho/age_sistema.jpeg",
                 "../../assets/image/trabalho/age_app.jpeg"),

    new Cases('sistema-imobiliario',
              'Sistema Imobiliário',
              `Desenvolvemos um sistema web complexo para uma empresa imobiliária.
              O sistema possuia diversas interações e muitas funcionalidades...`,
              
              `O sistema antigo não estava atendendo as necessidades do cliente e não era customizado,
               faltando funções necessárias para o seu dia a dia.
              Diante dessa necessidade, foi desenvolvido um sistema para atender as necessidades da empresa contratante.
              Um sistema que controla os imóveis, seus locatários e locadores.
              Além disso, o sistema tem integração com o banco de dados,
              gera relatórios solicitados pelo cliente entre outras utilidades.`,

              'Mar 21, 2017',
              "../../assets/image/artigo.jpg",
              "../../assets/image/trabalho/mockup_advogados.jpg",
              ""),

    new Cases('sistema-persotec',
              'Sistema Persotec',
              `Visando praticidade e funcionalidade,
              foi desenvolvido um site utilizando as tecnologias mais atuais disponíveis no mercado.`,

              `Visando praticidade e funcionalidade, foi desenvolvido um site que atendesse as necessidades do cliente utilizando as tecnologias mais atuais disponíveis no mercado.
              A responsividade foi uma das prioridades no desenvolvimento do projeto, conseguindo responder ao tamanho da tela para se adequar da melhor forma.
              Além disso, temos sempre a preocupação de deixar o site intuitivo e fácil navegação para o usuário.`,

              'Jul 21, 2017',
              "../../assets/image/persotec_logo.png",
              "../../assets/image/trabalho/mockups_persortec_sistema.jpeg",
              "../../assets/image/trabalho/mockups_persorte_app.jpeg"),

    new Cases('jovem-promissor',
              'Jovem Promissor',
              `Desenvolvemos um sistema de blog responsivo sobre investimento de renda financeira.
                Além da consultoria para o marketing digital da marca...`,

               `Foi desenvolvido um projeto onde auxiliamos o cliente em todas as etapas dando o suporte e ajuda para criação de seu blog.
                Migramos o domínio, criamos a logo, landing page e um formulário com envio automático de e-mail.
                Além da consultoria de marketing digital para expandir sua marca.
               A criação do blog foi pelo WordPress por solicitação do cliente devido ao custo ser mais acessível.`,
              'Out 20, 2017',
              "../../assets/image/jovempromissor.png",
              "../../assets/image/trabalho/mockups_jp.jpeg",
              ""),

    new Cases('regu-pecas',
              'ReguPeças',
              `Desenvolvemos um sistema de blog responsivo sobre investimento de renda financeira.
                Além da consultoria para o marketing digital da marca...`,
                `Foi construído uma página de serviços customizada e responsiva de uma oficina.
                 Após isso, foi feita uma consultoria completa de marketing digital para o aumento de acessos da página.`,
              'Jan 20, 2018',
              "../../assets/image/trabalho/mockups_regupecas_sistema.jpeg",
              "../../assets/image/trabalho/mockups_regupecas_sistema.jpeg",
              "../../assets/image/trabalho/mockups_regupecas_app.jpeg"),

    new Cases('bike-bonus',
              'Bike Bonus',
              `Foi construído uma página comercial customizada e responsiva de um aplicativo que está por vir.`,
              'Foi construído uma página comercial customizada e responsiva de um aplicativo que está por vir.',
              'Jan 20, 2018',
              "../../assets/image/smartmockups_jihvhe06.jpeg",
              "../../assets/image/smartmockups_jihvhe06.jpeg",
              "../../assets/image/smartmockups_jihvvzy8.jpeg")

  ];


  constructor() { }

}
