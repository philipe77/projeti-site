import { Component, OnInit, Input } from '@angular/core';
import { GlobalCasesService } from '../../site-shared/global-cases.service';
import { ActivatedRoute } from '@angular/router';
import { Cases } from '../../site-shared/cases.model';


@Component({
  selector: 'app-cases-detalhe',
  templateUrl: './cases-detalhe.component.html',
  styleUrls: ['./cases-detalhe.component.css'],
  providers:[GlobalCasesService]
})
export class CasesDetalheComponent implements OnInit {

  
  
  id:string;
  index:number;

  constructor(private globals: GlobalCasesService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.queryParams.subscribe(params => {
          this.id = params['id'];
      });
      for(let i=0; i<globals.cases_model.length; i++){
        if(globals.cases_model[i].id === this.id){       
          this.index = i;
          break;  
        }
      }
      
  }

  ngOnInit() {
    
  }

}
