import { Component, OnInit } from '@angular/core';
import { IMessage, SendEmailService } from '../../site/site-shared/send-email.service';

@Component({
  selector: 'app-contato',
  templateUrl: './contato.component.html',
  styleUrls: ['./contato.component.css'],
  providers:[SendEmailService]
})
export class ContatoComponent implements OnInit {

  msgEmailEnviado:boolean = false;
  msgErroEmail:boolean = false;

  message: IMessage = {};
  
  constructor(private appService: SendEmailService) { }
  
  ngOnInit() {
    console.log(this.msgEmailEnviado);  
  }
  
  //Envio de email
  sendEmail(message: IMessage) {
    this.appService.sendEmail(message).subscribe(res => {
      this.msgEmailEnviado = true;
    }, error => {
      this.msgErroEmail = true;
      
    })
  }

}
