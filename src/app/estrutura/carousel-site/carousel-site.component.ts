import { Component, OnInit } from '@angular/core';
import { NguCarousel } from '@ngu/carousel';
import { GlobalCasesService } from '../../site/site-shared/global-cases.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'sample',
  template: `
    <ngu-carousel
        [inputs]="carouselOne"
        (carouselLoad)="myfunc($event)">
          <ngu-item NguCarouselItem *ngFor="let item of item_cases">
          
          <div class="row mt-5">
      <div class="col-md-12" >
        <div class="card mb-3 blog-item-wrapper">
          <img class="card-img-top" src="{{casesGlobal.cases_model[item].imageOficial}}" height="270" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title">{{casesGlobal.cases_model[item].titulo}}</h5>
            <p class="card-text">{{casesGlobal.cases_model[item].descricao_resumida}}
              <a routerLink="cases-detalhe"(click)="selectCase(casesGlobal.cases_model[item].id)" >
                <strong>SAIBA MAIS</strong>
              </a>
            </p>
            <p class="card-text">
              <small class="text-muted">{{casesGlobal.cases_model[item].data}}</small>
            </p>
          </div>
        </div>
      </div>
    </div>
          
          </ngu-item>
          <button NguCarouselPrev class='leftRs'>&lt;</button>
          <button NguCarouselNext class='rightRs'>&gt;</button>
    </ngu-carousel>
  `,
  styles: [`
  .leftRs {
    position: absolute;
    margin: auto;
    top: 0;
    bottom: 0;
    width: 50px;
    height: 50px;
    box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);
    border-radius: 999px;
    left: 0;
}

.rightRs {
    position: absolute;
    margin: auto;
    top: 0;
    bottom: 0;
    width: 50px;
    height: 50px;
    box-shadow: 1px 2px 10px -1px rgba(0, 0, 0, .3);
    border-radius: 999px;
    right: 0;
}
  `]
})
export class CarouselSiteComponent implements OnInit {

  public carouselOne: NguCarousel;
  item_cases:Array<number> = [];
  size_cases:number;
  numero:number  

  //Criando Array de cases únicos
  addUniqueCases(){
    while(this.item_cases.length< this.size_cases){
     this.numero = this.randomise();         
      while(this.item_cases.indexOf(this.numero) != -1){
        this.numero = this.randomise();
      }
    this.item_cases.push(this.numero); 
    }
  }
  
  constructor(private casesGlobal:GlobalCasesService,
              private route: ActivatedRoute,
              private router: Router){
       //Quantidade de cases
       this.size_cases = casesGlobal.cases_model.length;
  } 

   //método randominco de acordo com o tamanho de cases
   randomise(): number {
    return Math.floor(Math.random() * (this.size_cases - 0)) + 0;
  }


  ngOnInit() {

  //Zera o array de itens case para não dar loop infinito
  this.item_cases=[];
  //Adiciona array radomico 
    this.addUniqueCases();  

    this.carouselOne = {
      grid: {xs: 1, sm: 1, md: 2, lg: 3, all: 0},
      slide: 1,
      speed: 400,
      interval: 4000,
      point: {
        visible: true
      },
      load: 2,
      touch: true,
      loop: true,
      custom: 'banner',
      animation: 'lazy',
    }
  }

  //Ao clickar no botão levará para pagina "Cases-detalhe" com parametro do id
  selectCase(id: number) {
    this.router.navigate(['/cases-detalhe'], { queryParams: { id: id } });
    window.scrollTo(0,0);
  }

  public myfunc(event: Event) {
     // carouselLoad will trigger this funnction when your load value reaches
     // it is helps to load the data by parts to increase the performance of the app
     // must use feature to all carousel
  }


}