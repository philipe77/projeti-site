import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Cases } from '../../site/site-shared/cases.model';
import { GlobalCasesService } from '../../site/site-shared/global-cases.service';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-case',
  templateUrl: './case.component.html',
  styleUrls: ['./case.component.css'],
  providers:[GlobalCasesService]
})
export class CasesComponent implements OnInit {

  items :Array<Cases>;

  constructor(private globals: GlobalCasesService,
    private route: ActivatedRoute,
    private router: Router) {}

    selectCase(id: number) {
      this.router.navigate(['/cases-detalhe'], { queryParams: { id: id } });
      window.scrollTo(0,0);
   }

  ngOnInit() {
    
    
  }

}
