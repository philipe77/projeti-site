import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms/';
import { NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';

//componentes 
import { MenuSuperiorComponent } from './estrutura/menu-superior/menu-superior.component';
import { FooterComponent } from './estrutura/footer/footer.component';
import { CasesComponent } from './estrutura/case/case.component';
import { ContatoComponent } from './estrutura/contato/contato.component';

//paginas
import { SiteComponent } from './site/site.component';
import { BlogComponent } from './blog/blog.component';
import { TodosCasesComponent } from './site/todos-cases/todos-cases.component';
import { CasesDetalheComponent } from './site/todos-cases/cases-detalhe/cases-detalhe.component';
import { ContatoPagComponent } from './site/contato-pag/contato-pag.component';
import { AdminBlogComponent } from './blog/admin-blog/admin-blog.component';

//Carousel
import { CarouselSiteComponent } from './estrutura/carousel-site/carousel-site.component';
import { NguCarouselModule } from '@ngu/carousel';



const appRoutes: Routes =[
  {path:'', component: SiteComponent},
  {path:'cases',component: TodosCasesComponent},
  {path:'cases-detalhe',component: CasesDetalheComponent},
  {path:'contato',component:ContatoPagComponent }
]


@NgModule({
  declarations: [
    AppComponent,
    SiteComponent,
    BlogComponent,
    AdminBlogComponent,
    MenuSuperiorComponent,
    FooterComponent,
    CasesComponent,
    CarouselSiteComponent,
    TodosCasesComponent,
    CasesDetalheComponent,
    ContatoPagComponent,
    ContatoComponent
  ],
  imports: [
    NguCarouselModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
